// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/Maatami/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Aaghosh Mein Jis Ko Paala Pardes Mein Us Ko Maara",
            "length": "13:47",
            "file": "Aaghosh Mein Jis Ko Paala Pardes Mein Us Ko Maara"
        }, {
            "track": 2,
            "name": "Aashur Ke Din Sheh Ko Kya Kya Nazar Aata He",
            "length": "2:45",
            "file": "Aashur Ke Din Sheh Ko Kya Kya Nazar Aata He"
        }, {
            "track": 3,
            "name": "Aashur Ke Suraj Kuchh To Bata",
            "length": "10:36",
            "file": "Aashur Ke Suraj Kuchh To Bata"
        }, {
            "track": 4,
            "name": "Aashur Ko Jiska Halq Kata Ham Uska Maatam Karte Hein",
            "length": "1:05",
            "file": "Aashur Ko Jiska Halq Kata Ham Uska Maatam Karte Hein"
        }, {
            "track": 5,
            "name": "Aashura Ke Roz E Bapa",
            "length": "10:42",
            "file": "Aashura Ke Roz E Bapa"
        }, {
            "track": 6,
            "name": "Aati Thi Sada Ae Zahra Beta Maara Gaya",
            "length": "10:44",
            "file": "Aati Thi Sada Ae Zahra Beta Maara Gaya"
        }, {
            "track": 7,
            "name": "Abbas Tujhe Ehle Wafa Yaad Karenge - Aashur Ki Woh Raat Woh Sanate Ka Aalam",
            "length": "5:10",
            "file": "Abbas Tujhe Ehle Wafa Yaad Karenge - Aashur Ki Woh Raat Woh Sanate Ka Aalam"
        }, {
            "track": 8,
            "name": "Ae Waae Asghar Laadaliya",
            "length": "6:18",
            "file": "Ae Waae Asghar Laadaliya"
        }, {
            "track": 9,
            "name": "Asr E Aashur Tha Shabbir The Tanhaai Thi - Garm Reti Pe Mein Girta Hun Sambhaalo Amma",
            "length": "2:43",
            "file": "Asr E Aashur Tha Shabbir The Tanhaai Thi - Garm Reti Pe Mein Girta Hun Sambhaalo Amma"
        }, {
            "track": 10,
            "name": "Bahatar Hein Is Taraf",
            "length": "6:02",
            "file": "Bahatar Hein Is Taraf"
        }, {
            "track": 11,
            "name": "Bekaso Ka Qaafla Ja Raha He Shaam Ko",
            "length": "6:28",
            "file": "Bekaso Ka Qaafla Ja Raha He Shaam Ko"
        }, {
            "track": 12,
            "name": "Bhul Jaaenge Sab Kuchh Karbala Na Bhulenge",
            "length": "1:17",
            "file": "Bhul Jaaenge Sab Kuchh Karbala Na Bhulenge"
        }, {
            "track": 13,
            "name": "Darya Pe Shor He Ke Alamdaar Mar Gaya",
            "length": "1:50",
            "file": "Darya Pe Shor He Ke Alamdaar Mar Gaya"
        }, {
            "track": 14,
            "name": "Ehle Haram Ko Lekar Shabbir Aa Rahe He",
            "length": "2:15",
            "file": "Ehle Haram Ko Lekar Shabbir Aa Rahe He"
        }, {
            "track": 15,
            "name": "Farzand E Fatema Ka Bhara Ghar Ujar Gaya ",
            "length": "1:36",
            "file": "Farzand E Fatema Ka Bhara Ghar Ujar Gaya "
        }, {
            "track": 16,
            "name": "Fatema Aap Ke Dil Ko He Dukhaaya Kis Ne",
            "length": "2:00",
            "file": "Fatema Aap Ke Dil Ko He Dukhaaya Kis Ne"
        }, {
            "track": 17,
            "name": "Haae Karbala Waalu - Shah Ke Ashab The",
            "length": "4:47",
            "file": "Haae Karbala Waalu - Shah Ke Ashab The"
        }, {
            "track": 18,
            "name": "Haae Sayyed E Abraar Tin Din Se Pyaasa He",
            "length": "1:17",
            "file": "Haae Sayyed E Abraar Tin Din Se Pyaasa He"
        }, {
            "track": 19,
            "name": "Har Ek Tere Gham Mein",
            "length": "11:20",
            "file": "Har Ek Tere Gham Mein"
        }, {
            "track": 20,
            "name": "He Fatema Ke Jigar Ka Tukra Husain Aaqa Husain Maula",
            "length": "9:48",
            "file": "He Fatema Ke Jigar Ka Tukra Husain Aaqa Husain Maula"
        }, {
            "track": 21,
            "name": "Husain Imaam Ka Gham He Ae Mumeno Aao (Mustafa Badri)",
            "length": "8:43",
            "file": "Husain Imaam Ka Gham He Ae Mumeno Aao (Mustafa Badri)"
        }, {
            "track": 22,
            "name": "Husain Ka Gham Makaan Makaan Mein",
            "length": "8:03",
            "file": "Husain Ka Gham Makaan Makaan Mein"
        }, {
            "track": 23,
            "name": "Karbal Tu Ne Lut Liya Ghar Zahra Ka",
            "length": "1:40",
            "file": "Karbal Tu Ne Lut Liya Ghar Zahra Ka"
        }, {
            "track": 24,
            "name": "Karbala Ma Su Bala Ma Mustafa Ni Aal Chhe (Version 1)",
            "length": "1:46",
            "file": "Karbala Ma Su Bala Ma Mustafa Ni Aal Chhe (Version 1)"
        }, {
            "track": 25,
            "name": "Karbala Ma Su Bala Ma Mustafa Ni Aal Chhe (Version 2)",
            "length": "11:57",
            "file": "Karbala Ma Su Bala Ma Mustafa Ni Aal Chhe (Version 2)"
        }, {
            "track": 26,
            "name": "Kehti Thi Baanu Asgar Pyaare Jhula Tera Khaali He",
            "length": "7:42",
            "file": "Kehti Thi Baanu Asgar Pyaare Jhula Tera Khaali He"
        }, {
            "track": 27,
            "name": "Koi Husain Sa Duniya Mein Dusra To Nahin",
            "length": "11:28",
            "file": "Koi Husain Sa Duniya Mein Dusra To Nahin"
        }, {
            "track": 28,
            "name": "Kufa Ma Thayo Aa Shor O Buka Mehraab Ma Haidar Ne Maara",
            "length": "7:47",
            "file": "Kufa Ma Thayo Aa Shor O Buka Mehraab Ma Haidar Ne Maara"
        }, {
            "track": 29,
            "name": "Laachaar Husaina Be Yaar Husaina",
            "length": "5:47",
            "file": "Laachaar Husaina Be Yaar Husaina"
        }, {
            "track": 30,
            "name": "Maara Gaya Bhai Ran Mein Zainab Jaaein Kahaan",
            "length": "11:40",
            "file": "Maara Gaya Bhai Ran Mein Zainab Jaaein Kahaan"
        }, {
            "track": 31,
            "name": "Maatam Karo Husain AS No",
            "length": "2:25",
            "file": "Maatam Karo Husain AS No"
        }, {
            "track": 32,
            "name": "Maatam Karo Mumeeno",
            "length": "1:17",
            "file": "Maatam Karo Mumeeno"
        }, {
            "track": 33,
            "name": "Maatam Ki Saf Bichhi He ",
            "length": "2:54",
            "file": "Maatam Ki Saf Bichhi He "
        }, {
            "track": 34,
            "name": "Madinat Jadena La Taqbalina - Qubul Hamko Na Karna Ae Madina",
            "length": "5:55",
            "file": "Madinat Jadena La Taqbalina - Qubul Hamko Na Karna Ae Madina"
        }, {
            "track": 35,
            "name": "Mazlum Ya Husaina Mazlum Ya Husaina",
            "length": "5:33",
            "file": "Mazlum Ya Husaina Mazlum Ya Husaina"
        }, {
            "track": 36,
            "name": "Mein Husain Hu Wo Husain Hu",
            "length": "10:29",
            "file": "Mein Husain Hu Wo Husain Hu"
        }, {
            "track": 37,
            "name": "Nabi Ka Ghar Jalaaya Ja Raha He ",
            "length": "1:16",
            "file": "Nabi Ka Ghar Jalaaya Ja Raha He "
        }, {
            "track": 38,
            "name": "Nabi Ke Kandho Pe Charhne Waala Pisar Ka Laasha Utha Raha He",
            "length": "9:14",
            "file": "Nabi Ke Kandho Pe Charhne Waala Pisar Ka Laasha Utha Raha He"
		}, {
            "track": 39,
            "name": "Nabi O Zahra Ka Naaz Uthaana",
            "length": "9:59",
            "file": "Nabi O Zahra Ka Naaz Uthaana"
        }, {
            "track": 40,
            "name": "Nana Ko Diya Tha Jo Waada",
            "length": "11:00",
            "file": "Nana Ko Diya Tha Jo Waada"
        }, {
            "track": 41,
            "name": "Paani To Pilaane Tujhe Jaaenga Sakina",
            "length": "7:47",
            "file": "Paani To Pilaane Tujhe Jaaenga Sakina"
        }, {
            "track": 42,
            "name": "Pukaari Zainab E Muztar Mere Husain Aao",
            "length": "10:09",
            "file": "Pukaari Zainab E Muztar Mere Husain Aao"
        }, {
            "track": 43,
            "name": "Pyaare Baaba Tum Laut Aao",
            "length": "1:48",
            "file": "Pyaare Baaba Tum Laut Aao"
        }, {
            "track": 44,
            "name": "Raaj Dulaara Zahra Ka Mehmaan Ban Kar Aaya He",
            "length": "8:06",
            "file": "Raaj Dulaara Zahra Ka Mehmaan Ban Kar Aaya He"
        }, {
            "track": 45,
            "name": "Raaj Dulaara Zahra Ka Zakhmi He Aur Pyaasa He",
            "length": "1:43",
            "file": "Raaj Dulaara Zahra Ka Zakhmi He Aur Pyaasa He"
        }, {
            "track": 46,
            "name": "Sajdo Mein Pehla Sajda Jaan E Zahra Ka Sajda",
            "length": "7:45",
            "file": "Sajdo Mein Pehla Sajda Jaan E Zahra Ka Sajda"
        }, {
            "track": 47,
            "name": "Sakina Shabbir Na Chhe Dukhtar Salaam Ye Par",
            "length": "5:19",
            "file": "Sakina Shabbir Na Chhe Dukhtar Salaam Ye Par"
        }, {
            "track": 48,
            "name": "Sar Khole Apne Laal Ko Roti He Fatema",
            "length": "5:18",
            "file": "Sar Khole Apne Laal Ko Roti He Fatema"
        }, {
            "track": 49,
            "name": "Sarwar Ko Maqtal Mein Aada Ne Ghera He Abbas Aa Jaao Bhai Akela He",
            "length": "8:41",
            "file": "Sarwar Ko Maqtal Mein Aada Ne Ghera He Abbas Aa Jaao Bhai Akela He"
        }, {
            "track": 50,
            "name": "Sehra E Karbala Mein Ab Asr Ki Ghari He",
            "length": "3:44",
            "file": "Sehra E Karbala Mein Ab Asr Ki Ghari He"
        }, {
            "track": 51,
            "name": "Shabbir He Mehw E Yaad E Khuda Ek Raat Ki Duniya Baaqi He",
            "length": "7:47",
            "file": "Shabbir He Mehw E Yaad E Khuda Ek Raat Ki Duniya Baaqi He"
        }, {
            "track": 52,
            "name": "Shabbir Teri Mazloomi Par ",
            "length": "9:38",
            "file": "Shabbir Teri Mazloomi Par "
        }, {
            "track": 53,
            "name": "Shahe Waala Ne Farmaaya Bahan Zainab Na Ghabraana",
            "length": "7:28",
            "file": "Shahe Waala Ne Farmaaya Bahan Zainab Na Ghabraana"
        }, {
            "track": 54,
            "name": "Shimr E Abtar Butha Khanjar Haath Mein Lekar Kaat Liya Sar - Haae Husaina Waae Husain",
            "length": "2:56",
            "file": "Shimr E Abtar Butha Khanjar Haath Mein Lekar Kaat Liya Sar - Haae Husaina Waae Husain"
        }, {
            "track": 55,
            "name": "Sibt E Mustafa Par Ye Kaisi Zulmraani He",
            "length": "6:55",
            "file": "Sibt E Mustafa Par Ye Kaisi Zulmraani He"
        }, {
            "track": 56,
            "name": "Suraj Se Zara Keh Do Parde Mein Chala Jaae (Ashara live)",
            "length": "7:44",
            "file": "Suraj Se Zara Keh Do Parde Mein Chala Jaae (Ashara live)"
        }, {
            "track": 57,
            "name": "Tashna Dahan Husain Garibul Watan Husain",
            "length": "4:08",
            "file": "Tashna Dahan Husain Garibul Watan Husain"
        }, {
            "track": 58,
            "name": "Tera Ghar Ho Gaya Viraana Ae Naana Fariyaad He",
            "length": "1:32",
            "file": "Tera Ghar Ho Gaya Viraana Ae Naana Fariyaad He"
        }, {
            "track": 59,
            "name": "Wa Mohammada Wa Musibata Binte Fatema Dar Bardar Phire",
            "length": "4:49",
            "file": "Wa Mohammada Wa Musibata Binte Fatema Dar Bardar Phire"
        }, {
            "track": 60,
            "name": "Ye Maatam Sada Rahe",
            "length": "6:43",
            "file": "Ye Maatam Sada Rahe"
        }, {
            "track": 61,
            "name": "Ye Na Pucho Gham E Husain He Kya",
            "length": "12:28",
            "file": "Ye Na Pucho Gham E Husain He Kya"
        }, {
            "track": 62,
            "name": "Ye To Mazlum Ka Maatam He Kam Na Hoga (Shamun Jamaali)",
            "length": "9:59",
            "file": "Ye To Mazlum Ka Maatam He Kam Na Hoga (Shamun Jamaali)"
        }, {
            "track": 63,
            "name": "Zainab Ki Asseri Ka Kya Haal Kaha Jaae",
            "length": "9:03",
            "file": "Zainab Ki Asseri Ka Kya Haal Kaha Jaae"
        }, {
            "track": 64,
            "name": "Zainab Ko Guma Kab Tha Ek Hashr Bapa Hoga",
            "length": "2:41",
            "file": "Zainab Ko Guma Kab Tha Ek Hashr Bapa Hoga"
        }, {
            "track": 65,
            "name": "Zalzalo Mein Duniya Thi Aasma Larazta Tha",
            "length": "6:43",
            "file": "Zalzalo Mein Duniya Thi Aasma Larazta Tha"
        } ],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});