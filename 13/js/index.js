// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/OhbatMajlis13/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Surah Al-Fatiha",
            "length": "00:48",
            "file": "01.Surah Al-Fatiha"
        }, {
            "track": 2,
            "name": "Surah An-Nas",
            "length": "00:40",
            "file": "02.Surah An-Nas"
        }, {
            "track": 3,
            "name": "Surah Al-Falaq",
            "length": "00:26",
            "file": "03.Surah Al-Falaq"
        }, {
            "track": 4,
            "name": "Surah Al-Ikhlas",
            "length": "00:17",
            "file": "04.Surah Al-Ikhlas"
        }, {
            "track": 5,
            "name": "Surah Al-Masad",
            "length": "00:29",
            "file": "05.Surah Al-Masad"
        }, {
            "track": 6,
            "name": "Surah An-Nasr",
            "length": "00:27",
            "file": "06.Surah An-Nasr"
        }, {
            "track": 7,
            "name": "Surah Al-Kafirun",
            "length": "00:31",
            "file": "07.Surah Al-Kafirun"
        }, {
            "track": 8,
            "name": "Surah Al-Kauthar",
            "length": "00:17",
            "file": "08.Surah Al-Kauthar"
        }, {
            "track": 9,
            "name": "Surah Al-Ma_un",
            "length": "00:38",
            "file": "09.Surah Al-Ma_un"
        }, {
            "track": 10,
            "name": "Surah Quraish",
            "length": "00:29",
            "file": "10.Surah Quraish"
        }, {
            "track": 11,
            "name": "Surah Al-Fil",
            "length": "00:33",
            "file": "11.Surah Al-Fil"
        }, {
            "track": 12,
            "name": "Surah Al-Humazah",
            "length": "00:42",
            "file": "12.Surah Al-Humazah"
        }, {
            "track": 13,
            "name": "Surah Al-_Asr",
            "length": "00:21",
            "file": "13.Surah Al-_Asr"
        }, {
            "track": 14,
            "name": "Ya Sayyeda Shohadaai",
            "length": "15:45",
            "file": "14.Ya Sayyeda Shohadaai"
        }, {
            "track": 15,
            "name": "Abkika Maulayal Husain",
            "length": "30:43",
            "file": "15.Abkika Maulayal Husain"
        }, {
            "track": 16,
            "name": "Gham Ma Husain Na Ro Je Biraadar",
            "length": "05:26",
            "file": "16.Gham Ma Husain Na Ro Je Biraadar"        
        }, {
            "track": 17,
            "name": "Tanhaai Mein Kaise Uthega Akbar Ka Janaza Rehne Do",
            "length": "07:01",
            "file": "17.Tanhaai Mein Kaise Uthega Akbar Ka Janaza Rehne Do"
        }, {
            "track": 18,
            "name": "Maatam Karo Husain AS No",
            "length": "02:25",
            "file": "18.Maatam Karo Husain AS No"
        }, {
            "track": 19,
            "name": "La Sabro Fika Walal Azao Jamilo",
            "length": "06:42",
            "file": "19.La Sabro Fika Walal Azao Jamilo"
        }, {
            "track": 20,
            "name": "Aashur Ke Suraj Kuchh To Bata",
            "length": "10:36",
            "file": "20.Aashur Ke Suraj Kuchh To Bata"
        }, {
            "track": 21,
            "name": "Ae Dil Tujhe Tehsil E Sa'adat Ka Jo Ham Ho",
            "length": "06:08",
            "file": "21.Ae Dil Tujhe Tehsil E Sa'adat Ka Jo Ham Ho"    }],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});