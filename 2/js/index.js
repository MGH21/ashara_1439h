// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/Salaam_201708/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Aada Ne Karbala Mein Sheh Ko Sata Sata Ke",
            "length": "4:07",
            "file": "Aada Ne Karbala Mein Sheh Ko Sata Sata Ke"
        }, {
            "track": 2,
            "name": "Aankhon Se Meri Khoon",
            "length": "01:38",
            "file": "Aankhon Se Meri Khoon"
        }, {
            "track": 3,
            "name": "Aao Asghar Pyaare Jhulo",
            "length": "3:10",
            "file": "Aao Asghar Pyaare Jhulo"
        }, {
            "track": 4,
            "name": "Aaqa Ko Mere Eik Bhi Qatra Na De Saki",
            "length": "4:03",
            "file": "Aaqa Ko Mere Eik Bhi Qatra Na De Saki"
        }, {
            "track": 5,
            "name": "Aashur Ke Tapte Sehra Mein Islaam Ko Kaisa Lut Liya",
            "length": "8:56",
            "file": "Aashur Ke Tapte Sehra Mein Islaam Ko Kaisa Lut Liya"
        }, {
            "track": 6,
            "name": "Aawaaz Aa Rahi He Karb O Bala Ke Ban Se",
            "length": "7:32",
            "file": "Aawaaz Aa Rahi He Karb O Bala Ke Ban Se"
        }, {
            "track": 7,
            "name": "Ae Khaak Nashino Kya Kehna Kya Takht Mila Kya Taaj Mila",
            "length": "7:19",
            "file": "Ae Khaak Nashino Kya Kehna Kya Takht Mila Kya Taaj Mila"
        }, {
            "track": 8,
            "name": "Ae Zamin E Karbala Teri To Aabaadi Hui",
            "length": "4:01",
            "file": "Ae Zamin E Karbala Teri To Aabaadi Hui"
        }, {
            "track": 9,
            "name": "Akbar Ki Laash Uthaae Shabbir Karbala Mein",
            "length": "6:35",
            "file": "Akbar Ki Laash Uthaae Shabbir Karbala Mein"
        }, {
            "track": 10,
            "name": "Alam Shabbir Ka Taf Mein Alam Ban Kar Nazar Aaya",
            "length": "8:04",
            "file": "Alam Shabbir Ka Taf Mein Alam Ban Kar Nazar Aaya"
        }, {
            "track": 11,
            "name": "Arsh Girne Laga Farsh Hilne Laga (Murgha)",
            "length": "11:09",
            "file": "Arsh Girne Laga Farsh Hilne Laga (Murgha)"
        }, {
            "track": 12,
            "name": "Asghar Ki Tashnagi Pe Aansu Baha Baha Ke",
            "length": "7:36",
            "file": "Asghar Ki Tashnagi Pe Aansu Baha Baha Ke"
        }, {
            "track": 13,
            "name": "Baanu Ka Jigar Tharaae Na Kyun",
            "length": "4:34",
            "file": "Baanu Ka Jigar Tharaae Na Kyun"
        }, {
            "track": 14,
            "name": "Baanu Ne Kaha Ro Kar Turbat Pe Sada Dungi",
            "length": "9:14",
            "file": "Baanu Ne Kaha Ro Kar Turbat Pe Sada Dungi"
        }, {
            "track": 15,
            "name": "Baanu Pukaari Roi Chhe Naakhi Ne Maatha Ma Dhul",
            "length": "20:22",
            "file": "Baanu Pukaari Roi Chhe Naakhi Ne Maatha Ma Dhul"
        }, {
            "track": 16,
            "name": "Bain The Sakina Ke Shaam Ho Ne Waali He",
            "length": "5:45",
            "file": "Bain The Sakina Ke Shaam Ho Ne Waali He"
        }, {
            "track": 17,
            "name": "Ban Ban Roe Baano",
            "length": "9:59",
            "file": "Ban Ban Roe Baano"
        }, {
            "track": 18,
            "name": "Behte Hue Aansu Mere Behte Hi To Jaana Gham Mein Mere Maula Ke Tu Dariya Ko Bahaana (Shk Ali Asgar)",
            "length": "5:00",
            "file": "Behte Hue Aansu Mere Behte Hi To Jaana Gham Mein Mere Maula Ke Tu Dariya Ko Bahaana (Shk Ali Asgar)"
        }, {
            "track": 19,
            "name": "Boli Baanu Pyaare Asghar Shaam Bhai Ghar Aawo",
            "length": "6:07",
            "file": "Boli Baanu Pyaare Asghar Shaam Bhai Ghar Aawo"
        }, {
            "track": 20,
            "name": "Boli Zainab Ke Nahin Naaz Uthaane Waala",
            "length": "5:07",
            "file": "Boli Zainab Ke Nahin Naaz Uthaane Waala"
        }, {
            "track": 21,
            "name": "Boli Zainab Salaam Lo Bhai Ja Rahi Hu Watan Utho Bhai",
            "length": "7:55",
            "file": "Boli Zainab Salaam Lo Bhai Ja Rahi Hu Watan Utho Bhai"
        }, {
            "track": 22,
            "name": "Dab Ke Ghoro Ke Sumu Se Sheh Ka Laasha Reh Na Jaae",
            "length": "8:26",
            "file": "Dab Ke Ghoro Ke Sumu Se Sheh Ka Laasha Reh Na Jaae"
        }, {
            "track": 23,
            "name": "Dubi Hui Dukh Ke Saagar Mein",
            "length": "2:26",
            "file": "Dubi Hui Dukh Ke Saagar Mein"
        }, {
            "track": 24,
            "name": "Dukhiya Banu Chillaawat He Asghar Ka Ham Se Chhurae Liyo",
            "length": "7:33",
            "file": "Dukhiya Banu Chillaawat He Asghar Ka Ham Se Chhurae Liyo"
        }, {
            "track": 25,
            "name": "Dukhiya Banu Ro Kar Boli Asghar Ka Khaali Jhula He",
            "length": "3:31",
            "file": "Dukhiya Banu Ro Kar Boli Asghar Ka Khaali Jhula He"
        }, {
            "track": 26,
            "name": "Ehle Jahaan Ye Dard Ka Manzar Na Bhulna",
            "length": "4:14",
            "file": "Ehle Jahaan Ye Dard Ka Manzar Na Bhulna"
        }, {
            "track": 27,
            "name": "Ek Daur Salaami Duniya Ka Sadiyo Mein Aisa Aata He",
            "length": "9:35",
            "file": "Ek Daur Salaami Duniya Ka Sadiyo Mein Aisa Aata He"
        }, {
            "track": 28,
            "name": "Farmaate The Har Eik Se Mein Be Qasoor Hoon ",
            "length": "7:30",
            "file": "Farmaate The Har Eik Se Mein Be Qasoor Hoon "
        }, {
            "track": 29,
            "name": "Gehwaare Se Husain Jo Asghar Ko Chale",
            "length": "13:30",
            "file": "Gehwaare Se Husain Jo Asghar Ko Chale"
        }, {
            "track": 30,
            "name": "Gul Ki Rangat Khun E Husaini Naghma E Bulbul Haae Husain",
            "length": "7:19",
            "file": "Gul Ki Rangat Khun E Husaini Naghma E Bulbul Haae Husain"
        }, {
            "track": 31,
            "name": "Guzar Gae The Kaeen Din Ke Ghar Mein Aab Na Tha",
            "length": "6:56",
            "file": "Guzar Gae The Kaeen Din Ke Ghar Mein Aab Na Tha"
        }, {
            "track": 32,
            "name": "Haae Aashur Ko Shabbir Ne Kya Kya Dekha",
            "length": "6:47",
            "file": "Haae Aashur Ko Shabbir Ne Kya Kya Dekha"
        }, {
            "track": 33,
            "name": "Haakim Ne Likha Shaam Ke Lashkar Ko Sunaana Paani Na Pilaana",
            "length": "15:31",
            "file": "Haakim Ne Likha Shaam Ke Lashkar Ko Sunaana Paani Na Pilaana"
        }, {
            "track": 34,
            "name": "Hadiya Qubul Karle",
            "length": "13:07",
            "file": "Hadiya Qubul Karle"
        }, {
            "track": 35,
            "name": "Hashr Ke Din Aasiyo",
            "length": "5:56",
            "file": "Hashr Ke Din Aasiyo"
        }, {
            "track": 36,
            "name": "Husain Jaisa Shahid E Aazam Jaha Mein Koi Huwa Nahi He",
            "length": "1:06",
            "file": "Husain Jaisa Shahid E Aazam Jaha Mein Koi Huwa Nahi He"
        }, {
            "track": 37,
            "name": "Husain Tumko Madina Salaam Kehta He",
            "length": "4:38",
            "file": "Husain Tumko Madina Salaam Kehta He"
        }, {
            "track": 38,
            "name": "Husaini Kaarwaan Pyaasa Tha Karbal Ke Bayabaan Mein",
            "length": "3:15",
            "file": "Husaini Kaarwaan Pyaasa Tha Karbal Ke Bayabaan Mein"
        }, {
            "track": 39,
            "name": "Imaan Ki Aayato Se Jangal Bhara Huwa He",
            "length": "10:46",
            "file": "Imaan Ki Aayato Se Jangal Bhara Huwa He"
        }, {
            "track": 40,
            "name": "Jab Ran Mein Husain Asghar E Be Sheer Ko Laae",
            "length": "8:59",
            "file": "Jab Ran Mein Husain Asghar E Be Sheer Ko Laae"
        }, {
            "track": 41,
            "name": "Jab Ujar Jaaega Zahra Ka Makaan Asr Ke Baad",
            "length": "9:05",
            "file": "Jab Ujar Jaaega Zahra Ka Makaan Asr Ke Baad"
        }, {
            "track": 42,
            "name": "Jag Ke Imaam Zainul Aba Sareban He",
            "length": "3:18",
            "file": "Jag Ke Imaam Zainul Aba Sareban He"
        }, {
            "track": 43,
            "name": "Jhula Asghar Ka Maadar Jhulaati Rahi",
            "length": "7:02",
            "file": "Jhula Asghar Ka Maadar Jhulaati Rahi"
        }, {
            "track": 44,
            "name": "Jhule Mein He Qaraar Na",
            "length": "1:55",
            "file": "Jhule Mein He Qaraar Na"
        }, {
            "track": 45,
            "name": "Kab Sheh Ko Raaste Mein Khayal E Qaza Na Tha",
            "length": "10:33",
            "file": "Kab Sheh Ko Raaste Mein Khayal E Qaza Na Tha"
        }, {
            "track": 46,
            "name": "Kaha Ro Ro Ke Zainab Ne Ilaahi Subah Kya Hoga",
            "length": "13:50",
            "file": "Kaha Ro Ro Ke Zainab Ne Ilaahi Subah Kya Hoga"
        }, {
            "track": 47,
            "name": "Kaha Zainab Ne Sarwar Se Ke Banu Ghul Machaati He (Shk Ali Asgar)",
            "length": "6:05",
            "file": "Kaha Zainab Ne Sarwar Se Ke Banu Ghul Machaati He (Shk Ali Asgar)"
        }, {
            "track": 48,
            "name": "Karbala Aa Ke Boli Ye Zainab Mere Maan Jaae Mein Tum Pe Waari (Surgical)",
            "length": "4:43",
            "file": "Karbala Aa Ke Boli Ye Zainab Mere Maan Jaae Mein Tum Pe Waari (Surgical)"
        }, {
            "track": 49,
            "name": "Karbala Ki Ajab Bayaani He Khun E Dil Sun Ke Paani Paani He",
            "length": "3:01",
            "file": "Karbala Ki Ajab Bayaani He Khun E Dil Sun Ke Paani Paani He"
        }, {
            "track": 50,
            "name": "Kehte The Zainul Aabedin Tum Ko Sata Ke Kya Mila",
            "length": "7:16",
            "file": "Kehte The Zainul Aabedin Tum Ko Sata Ke Kya Mila"
        }, {
            "track": 51,
            "name": "Kehti He Sakina Mere Baba Ko Bulaao",
            "length": "8:01",
            "file": "Kehti He Sakina Mere Baba Ko Bulaao"
        }, {
            "track": 52,
            "name": "Kehti The Eik Dukhiya Asghar Mera Kahaan He",
            "length": "6:11",
            "file": "Kehti The Eik Dukhiya Asghar Mera Kahaan He"
        }, {
            "track": 53,
            "name": "Khaate He Shaahe Waala Thokar Qadam Qadam Par",
            "length": "7:41",
            "file": "Khaate He Shaahe Waala Thokar Qadam Qadam Par"
        }, {
            "track": 54,
            "name": "Khuahar E Shaha Zainab Dukhtar E Ali Zainab",
            "length": "2:25",
            "file": "Khuahar E Shaha Zainab Dukhtar E Ali Zainab"
        }, {
            "track": 55,
            "name": "Khun Ki Nadiyo Se Bhi Ehle Wafa Guzar Gae",
            "length": "5:31",
            "file": "Khun Ki Nadiyo Se Bhi Ehle Wafa Guzar Gae"
        }, {
            "track": 56,
            "name": "Kya Bataein Ke Zainab Ki Kya Shaan He - Itrat E Nabi Jaan E Murtaza Khuahar E Husain Binte Fatema",
            "length": "08:36",
            "file": "Kya Bataein Ke Zainab Ki Kya Shaan He - Itrat E Nabi Jaan E Murtaza Khuahar E Husain Binte Fatema"
        }, {
            "track": 57,
            "name": "Kyu Na Ban Jaae Teri Shahaadat Sharaf",
            "length": "9:18",
            "file": "Kyu Na Ban Jaae Teri Shahaadat Sharaf"
        }, {
            "track": 58,
            "name": "Le Ke Ek Qaafela Ghamzadah Saarebaan Chal Raha He",
            "length": "8:47",
            "file": "Le Ke Ek Qaafela Ghamzadah Saarebaan Chal Raha He"
        }, {
            "track": 59,
            "name": "Likha He Ke Ansaar Jo ",
            "length": "11:05",
            "file": "Likha He Ke Ansaar Jo "
        }, {
            "track": 60,
            "name": "Maidaan Mein Koi Jaane Waala Na Raha",
            "length": "2:15",
            "file": "Maidaan Mein Koi Jaane Waala Na Raha"
        }, {
            "track": 61,
            "name": "Mashk O Alam Udaas He Darya Udaas He",
            "length": "4:33",
            "file": "Mashk O Alam Udaas He Darya Udaas He"
        }, {
            "track": 62,
            "name": "Mujhko Baaba Sada Do Agar Ho Sake",
            "length": "10:39",
            "file": "Mujhko Baaba Sada Do Agar Ho Sake"
        }, {
            "track": 63,
            "name": "Mujrai Khalq Mein In Aankho Ne Kya Kya Dekha",
            "length": "5:37",
            "file": "Mujrai Khalq Mein In Aankho Ne Kya Kya Dekha"
        }, {
            "track": 64,
            "name": "Mujrai Qaid Se Jab Aabid E Be Par Chhute",
            "length": "6:27",
            "file": "Mujrai Qaid Se Jab Aabid E Be Par Chhute"
        }, {
            "track": 65,
            "name": "Nanha Sa Ek Sipaahi Godi Mein Aa Raha He",
            "length": "11:2",
            "file": "Nanha Sa Ek Sipaahi Godi Mein Aa Raha He"
        }, {
            "track": 66,
            "name": "Neza Jigar Se Kheechna Puchhe Koi Husain Se",
            "length": "6:05",
            "file": "Neza Jigar Se Kheechna Puchhe Koi Husain Se"
        }, {
            "track": 67,
            "name": "Nishan E Fauj Payambar Sajaaya Jaata He",
            "length": "6:24",
            "file": "Nishan E Fauj Payambar Sajaaya Jaata He"
        }, {
            "track": 68,
            "name": "Paamaal Hoga Dasht E Sitam Mein Chaman Koi",
            "length": "3:40",
            "file": "Paamaal Hoga Dasht E Sitam Mein Chaman Koi"
        }, {
            "track": 69,
            "name": "Pardes Mein Aale Ehmad Ko Be Jurm O Khata Ko Lut Liya",
            "length": "4:28",
            "file": "Pardes Mein Aale Ehmad Ko Be Jurm O Khata Ko Lut Liya"
        }, {
            "track": 70,
            "name": "Piri Mein No Jawaan Ko Maidaan Se Laane Waale Kuchh Der Dam To Lele Maiyyit Uthaane Waale",
            "length": "7:59",
            "file": "Piri Mein No Jawaan Ko Maidaan Se Laane Waale Kuchh Der Dam To Lele Maiyyit Uthaane Waale"
        }, {
            "track": 71,
            "name": "Pohoncho Haram Madina Ma Karbal Ne Shaam Si",
            "length": "5:14",
            "file": "Pohoncho Haram Madina Ma Karbal Ne Shaam Si"
        }, {
            "track": 72,
            "name": "Pukaari Shaana Hila Ke Maadar Utho Sakina Sahar Hui He (Shk Ghulam Abbas Murgha)",
            "length": "6:05",
            "file": "Pukaari Shaana Hila Ke Maadar Utho Sakina Sahar Hui He (Shk Ghulam Abbas Murgha)"
        }, {
            "track": 73,
            "name": "Qatl Asgar Ho Gae Veeran Jhula Ho Gaya",
            "length": "3:36",
            "file": "Qatl Asgar Ho Gae Veeran Jhula Ho Gaya"
        }, {
            "track": 74,
            "name": "Qurbaan Gahe Haq Mein Bhai Ka Yu Haath Bataaya Zainab Ne",
            "length": "5:29",
            "file": "Qurbaan Gahe Haq Mein Bhai Ka Yu Haath Bataaya Zainab Ne"
        }, {
            "track": 75,
            "name": "an Mein Ye Sitam Aale Pyambar Pe Hua He",
            "length": "2:39",
            "file": "an Mein Ye Sitam Aale Pyambar Pe Hua He"
        }, {
            "track": 76,
            "name": "Ro Ke Kehti Thi Zainab Ye Ran Mein",
            "length": "5:27",
            "file": "Ro Ke Kehti Thi Zainab Ye Ran Mein"
        }, {
            "track": 77,
            "name": "Ro Ke Zainab Ne Kaha Reh Gae Sarwar Tanha",
            "length": "6:53",
            "file": "Ro Ke Zainab Ne Kaha Reh Gae Sarwar Tanha"
        }, {
            "track": 78,
            "name": "Ro Ro Ke Puchhti Hein Baanu Shahe Zaman Se",
            "length": "6:55",
            "file": "Ro Ro Ke Puchhti Hein Baanu Shahe Zaman Se"
        }, {
            "track": 79,
            "name": "Ro Ro Kehti Thi Baali Sakina Zalimo Mere Gohar Na Chhino",
            "length": "1:29",
            "file": "Ro Ro Kehti Thi Baali Sakina Zalimo Mere Gohar Na Chhino"
        }, {
            "track": 80,
            "name": "Role Gham E Shabbir Mein Kal Ho Ke Na Ho",
            "length": "1:15",
            "file": "Role Gham E Shabbir Mein Kal Ho Ke Na Ho"
        }, {
            "track": 81,
            "name": "Saahil Pe He Pyaasi",
            "length": "7:03",
            "file": "Saahil Pe He Pyaasi"
        }, {
            "track": 82,
            "name": "Salaam Khaak Nashino Pe Sogwaaro Ka",
            "length": "4:57",
            "file": "Salaam Khaak Nashino Pe Sogwaaro Ka"
        }, {
            "track": 83,
            "name": "Salaam Un Par Jo Aaghosh E Pidar Mein Ran Ko Jate He",
            "length": "9:43",
            "file": "Salaam Un Par Jo Aaghosh E Pidar Mein Ran Ko Jate He"
        }, {
            "track": 84,
            "name": "Salaam Un Par Jo Raah E Haq Mein Mataa E Hasti Luta Gae He",
            "length": "4:00",
            "file": "Salaam Un Par Jo Raah E Haq Mein Mataa E Hasti Luta Gae He"
        }, {
            "track": 85,
            "name": "Sawaari Dosh E Ehmad Par Shahid E Karbala Ki He",
            "length": "2:36",
            "file": "Sawaari Dosh E Ehmad Par Shahid E Karbala Ki He"
        }, {
            "track": 86,
            "name": "Shabbir Ke Seene Par Jab Shimr Charha Hoga",
            "length": "14:07",
            "file": "Shabbir Ke Seene Par Jab Shimr Charha Hoga"
        }, {
            "track": 87,
            "name": "Shabbir Ko Khanjar Ke Neeche Kya Janiye Kya Kya Yaad Aaya",
            "length": "10:29",
            "file": "Shabbir Ko Khanjar Ke Neeche Kya Janiye Kya Kya Yaad Aaya"
        }, {
            "track": 88,
            "name": "Sibt E Rasul E Aazam Mazlum Be Nawa He",
            "length": "8:22",
            "file": "Sibt E Rasul E Aazam Mazlum Be Nawa He"
        }, {
            "track": 89,
            "name": "Sibt E Sarkaar Do Aalam Ka Kata Sar Ran Mein",
            "length": "7:52",
            "file": "Sibt E Sarkaar Do Aalam Ka Kata Sar Ran Mein"
        }, {
            "track": 90,
            "name": "Tanhaai Mein Kaise Uthega Akbar Ka Janaza Rehne Do",
            "length": "7:01",
            "file": "Tanhaai Mein Kaise Uthega Akbar Ka Janaza Rehne Do"
        }, {
            "track": 91,
            "name": "Tha Kishwar E Iraaq Mein Ek Mard Ba Safa",
            "length": "10:38",
            "file": "Tha Kishwar E Iraaq Mein Ek Mard Ba Safa"
        }, {
            "track": 92,
            "name": "Tujhsa Koi Duniya Mein Pyaasa Na Raha Maula",
            "length": "3:39",
            "file": "Tujhsa Koi Duniya Mein Pyaasa Na Raha Maula"
        }, {
            "track": 93,
            "name": "Waajebur Rehm The Zinda Ke Sazaawaar Na The",
            "length": "6:37",
            "file": "Waajebur Rehm The Zinda Ke Sazaawaar Na The"
        }, {
            "track": 94,
            "name": "Ya Rab Sagir Sin Mein Koi Be Pidar Na Ho",
            "length": "3:27",
            "file": "Ya Rab Sagir Sin Mein Koi Be Pidar Na Ho"
        }, {
            "track": 95,
            "name": "Ya Rabba Bahaq Gohar Dandaan E Mustafa",
            "length": "9:35",
            "file": "Ya Rabba Bahaq Gohar Dandaan E Mustafa"
        }, {
            "track": 96,
            "name": "Yaad Asghar Ki Jab Dil Jalaane Lagi",
            "length": "5:03",
            "file": "Yaad Asghar Ki Jab Dil Jalaane Lagi"
        }, {
            "track": 97,
            "name": "Yazid Nehes Bhi He Mehfil E Sharab Bhi He",
            "length": "3:11",
            "file": "Yazid Nehes Bhi He Mehfil E Sharab Bhi He"
        }, {
            "track": 98,
            "name": "Ye Aasma Se Puchho Ye Kehkasha Se Pucho",
            "length": "13:12",
            "file": "Ye Aasma Se Puchho Ye Kehkasha Se Pucho"
        }, {
            "track": 99,
            "name": "Ye Kehte The Shahe Waala Sakina Ham Nahi Honge",
            "length": "7:18",
            "file": "Ye Kehte The Shahe Waala Sakina Ham Nahi Honge"
        }, {
            "track": 100,
            "name": "Ye Kiska Laal He Kis Dil Ka Yeh Sahaara He",
            "length": "2:53",
            "file": "Ye Kiska Laal He Kis Dil Ka Yeh Sahaara He"
        }, {
            "track": 101,
            "name": "Ye Pukaari Zainab E Khasta Tan",
            "length": "3:58",
            "file": "Ye Pukaari Zainab E Khasta Tan"
        }, {
            "track": 102,
            "name": "Zainab Ye Boli Hashr Ka Manzar Nazar Mein He",
            "length": "6:04",
            "file": "Zainab Ye Boli Hashr Ka Manzar Nazar Mein He"
        }, {
            "track": 103,
            "name": "Zamin E Karbala Par Aise Kuchh Manzar Nazar Aae",
            "length": "4:57",
            "file": "Zamin E Karbala Par Aise Kuchh Manzar Nazar Aae"       
		}],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});