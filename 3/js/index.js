// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/ArabiMaraasi/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Madinat Jadena La Taqbalina",
            "length": "6:43",
            "file": "00.Madinat Jadena La Taqbalina"
        }, {
            "track": 2,
            "name": "Huznun Azaaba Jawaanehi Wa Fuaadii",
            "length": "6:13",
            "file": "01.Huznun Azaaba Jawaanehi Wa Fuaadi"
        }, {
            "track": 3,
            "name": "Ya Imaamal Haqqe Maulaanal Husain",
            "length": "4:56",
            "file": "02.Ya Imaamal Haqqe Maulaanal Husain"
        }, {
            "track": 4,
            "name": "La Sabro Fika Walal Azao Jamilo",
            "length": "6:42",
            "file": "05.La Sabro Fika Walal Azao Jamilo"
        }, {
            "track": 5,
            "name": "Ya Husainan Wama Nasito Husaina",
            "length": "05:50",
            "file": "06.Ya Husainan Wama Nasito Husaina"
        }, {
            "track": 6,
            "name": "Ya Taffo Hayatka Sahaabul Junu",
            "length": "5:49",
            "file": "07.Ya Taffo Hayatka Sahaabul Junu"
        }, {
            "track": 7,
            "name": "Al Mim Be Ard E Taffe Wal Sam Turbahu",
            "length": "10:37",
            "file": "08.Al Mim Be Ard E Taffe Wal Sam Turbahu"
        }, {
            "track": 8,
            "name": "Qum Sahebi Fi Matamin Le Nanuha",
            "length": "3:20",
            "file": "09.Qum Sahebi Fi Matamin Le Nanuha"
        }, {
            "track": 9,
            "name": "Ya Qateelan Atshaana Fi Karbalai",
            "length": "9:07",
            "file": "10.Ya Qateelan Atshaana Fi Karbalai"
        }, {
            "track": 10,
            "name": "Noha - Alaike Ya Nainawa Bukaai",
            "length": "5:18",
            "file": "Noha - Alaike Ya Nainawa Bukaai"
        }, {
            "track": 11,
            "name": "Noha - Ayyo Khatbin Jaraa Fike Ya Karbala",
            "length": "7:45",
            "file": "Noha - Ayyo Khatbin Jaraa Fike Ya Karbala"
        }, {
            "track": 12,
            "name": "Noha - Bil Ahl E Wal Halime",
            "length": "5:21",
            "file": "Noha - Bil Ahl E Wal Halime"
        }, {
            "track": 13,
            "name": "Noha - Haza Husainun Fi Karbalain Maqtul",
            "length": "4:50",
            "file": "Noha - Haza Husainun Fi Karbalain Maqtul"
        }, {
            "track": 14,
            "name": "Noha - Husain Husaina Husain Husaina",
            "length": "2:25",
            "file": "Noha - Husain Husaina Husain Husaina"
        }, {
            "track": 15,
            "name": "Noha - Ilbasu Siyaabal Buka",
            "length": "5:28",
            "file": "Noha - Ilbasu Siyaabal Buka"
        }, {
            "track": 16,
            "name": "Noha - Inna Zikral Husain E Yadumu",
            "length": "7:28",
            "file": "Noha - Inna Zikral Husain E Yadumu"
        }, {
            "track": 17,
            "name": "Noha - Karbalaya Karbalaya",
            "length": "3:37",
            "file": "Noha - Karbalaya Karbalaya"
        }, {
            "track": 18,
            "name": "Noha - Ruz ul Husain E Huznun",
            "length": "3:27",
            "file": "Noha - Ruz ul Husain E Huznun"
        }, {
            "track": 19,
            "name": "Noha - Tabkil Husain Sakina Aya Husainao",
            "length": "6:15",
            "file": "Noha - Tabkil Husain Sakina Aya Husainao"
        }, {
            "track": 20,
            "name": "Noha - Ya Durrata Aini Alaa",
            "length": "8:26",
            "file": "Noha - Ya Durrata Aini Alaa"
        }, {
            "track": 21,
            "name": "Noha - Ya Husaina Husaina Husain Ya Husain",
            "length": "6:25",
            "file": "Noha - Ya Husaina Husaina Husain Ya Husain"
        }, {
            "track": 22,
            "name": "Noha - Yaale Nabde  Zainabe Bil Abaa",
            "length": "7:38",
            "file": "Noha - Yaale Nabde  Zainabe Bil Abaa"
        }, {
            "track": 23,
            "name": "Noha - Zainabun Baba Baite Hataati",
            "length": "8:14",
            "file": "Noha - Zainabun Baba Baite Hataati"
        }, {
            "track": 24,
            "name": "Noha - Zainabun Fis Sufufe Tunadi",
            "length": "2:54",
            "file": "Noha - Zainabun Fis Sufufe Tunadi (Miqdad BS-Qazi Dr. Shaikh Abbas Borhany)"
		}],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});