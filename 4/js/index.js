// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/Nasihat_201708/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Ilm Na Moti Jaro",
            "length": "5:26",
            "file": "01.Ilm Na Moti Jaro"
        }, {
            "track": 2,
            "name": "Allah Taala Nu Hamd Tu Karje",
            "length": "11:02",
            "file": "02.Allah Taala Nu Hamd Tu Karje"
        }, {
            "track": 3,
            "name": "Biraadar Tu Nasihat Sun",
            "length": "43:26",
            "file": "03.Biraadar Tu Nasihat Sun"
        }, {
            "track": 4,
            "name": "Chalo Ae Pyara Azizo Chalo",
            "length": "17:59",
            "file": "03.Chalo Ae Pyara Azizo Chalo"
        }, {
            "track": 5,
            "name": "Ae Bhaai Khauf Khuda No Tu Raakh",
            "length": "0:38",
            "file": "Ae Bhaai Khauf Khuda No Tu Raakh"
        }, {
            "track": 6,
            "name": "Buniyaad Aa Jahaan Ni",
            "length": "12:56",
            "file": "Buniyaad Aa Jahaan Ni"
        }, {
            "track": 7,
            "name": "Chit Dai Ne",
            "length": "2:02",
            "file": "Chit Dai Ne"
        }, {
            "track": 8,
            "name": "Dil Si Hasad Ne Nikaal",
            "length": "5:54",
            "file": "Dil Si Hasad Ne Nikaal"
        }, {
            "track": 9,
            "name": "Ek Pal Tu Mat Khuda Nei",
            "length": "23:01",
            "file": "Ek Pal Tu Mat Khuda Ne"
        }, {
            "track": 10,
            "name": "Gar Chaahe Tu Ae Mumin Uqba Ni Saadat Ne",
            "length": "6:37",
            "file": "Gar Chaahe Tu Ae Mumin Uqba Ni Saadat Ne"
        }, {
            "track": 11,
            "name": "Gar Maara Jisam Ni Aa Khaalo Ne Ukhaarun To",
            "length": "2:47",
            "file": "Gar Maara Jisam Ni Aa Khaalo Ne Ukhaarun To"
        }, {
            "track": 12,
            "name": "Je Hadesa Namud Thaya Che Jahaan Thi (Version 2)",
            "length": "59:11",
            "file": "Je Hadesa Namud Thaya Che Jahaan Thi (Version 2)"
        }, {
            "track": 13,
            "name": "Je Hadesa Namud Thaya Che Jahaan Thi",
            "length": "13:5Je Koi Bashar Ni6",
            "file": "Je Hadesa Namud Thaya Che Jahaan Thi"
        }, {
            "track": 14,
            "name": "Je Koi Bashar Ni",
            "length": "2:44",
            "file": "Je Koi Bashar Ni"
        }, {
            "track": 15,
            "name": "Je Koi Khuda Na Naam Ni Maala Japa Kare",
            "length": "3:59",
            "file": "Je Koi Khuda Na Naam Ni Maala Japa Kare"
        }, {
            "track": 16,
            "name": "Mat Bhulje Jigar Thi Mohammad Ni Aal Ne",
            "length": "5:21",
            "file": "Mat Bhulje Jigar Thi Mohammad Ni Aal Ne"
        }, {
            "track": 17,
            "name": "Moti Jawaahiro Thi",
            "length": "21:53",
            "file": "Moti Jawaahiro Thi"
        }, {
            "track": 18,
            "name": "Murshid Chhe Logo Khuda Na Baraabar",
            "length": "5:23",
            "file": "Murshid Chhe Logo Khuda Na Baraabar"
        }, {
            "track": 19,
            "name": "Saad Guru Farmawi Gaya",
            "length": "12:30",
            "file": "Saad Guru Farmawi Gaya"
        }, {
            "track": 20,
            "name": "Suni Le Dil Na Kaano Thi",
            "length": "34:32",
            "file": "Suni Le Dil Na Kaano Thi"
        }, {
            "track": 21,
            "name": "Waat Chhe Faedat Daar Azizo Waat Ne Maano",
            "length": "1:22",
            "file": "Waat Chhe Faedat Daar Azizo Waat Ne Maano"
        }],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});