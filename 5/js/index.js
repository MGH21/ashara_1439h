// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/Madaaih/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Allamal Insaana Ma Lam Ya Lam Ek Aezaaz He",
            "length": "21:27",
            "file": "00.Allamal Insaana Ma Lam Ya Lam Ek Aezaaz He"
        }, {
            "track": 2,
            "name": "Aah Bhar Kar Kaho Ya Husaina Gham E Shabbir Ki Dhab Yahi He ",
            "length": "6:47",
            "file": "Aah Bhar Kar Kaho Ya Husaina Gham E Shabbir Ki Dhab Yahi He "
        }, {
            "track": 3,
            "name": "Ae Shahe Aali Qadar Ham Aap Dil Qurbaan Hein",
            "length": "5:55",
            "file": "Ae Shahe Aali Qadar Ham Aap Dil Qurbaan Hein"
        }, {
            "track": 4,
            "name": "Ajab Taazgi Aaj Gulzaar Mein Hein",
            "length": "6:50",
            "file": "Ajab Taazgi Aaj Gulzaar Mein Hein"
        }, {
            "track": 5,
            "name": "Deen O Duniya Mere Huzur Se He",
            "length": "8:25",
            "file": "Deen O Duniya Mere Huzur Se He"
        }, {
            "track": 6,
            "name": "Haazal Fatah Saifulhuda Har Ilm Ke Neamal Ina ",
            "length": "4:04",
            "file": "Haazal Fatah Saifulhuda Har Ilm Ke Neamal Ina "
        }, {
            "track": 7,
            "name": "Husain Par Chashm Jo He Pur Nam Yeh Saifedeen Sheh Ka Mojizah He",
            "length": "7:35",
            "file": "Husain Par Chashm Jo He Pur Nam Yeh Saifedeen Sheh Ka Mojizah He"
        }, {
            "track": 8,
            "name": "Husn E Gulru Maah E Taabindah",
            "length": "4:30",
            "file": "Husn E Gulru Maah E Taabindah"
        }, {
            "track": 9,
            "name": "Labbaik Ya Daai Allah Labbaik Daai Al Husain",
            "length": "11:05",
            "file": "Labbaik Ya Daai Allah Labbaik Daai Al Husain"
        }, {
            "track": 10,
            "name": "Ladunni Ilm Na Behr E Atam Chhe",
            "length": "4:03",
            "file": "Ladunni Ilm Na Behr E Atam Chhe"
        }, {
            "track": 11,
            "name": "Maanind Andaleeb Ke",
            "length": "6:53",
            "file": "Maanind Andaleeb Ke"
        }, {
            "track": 12,
            "name": "Minnat Karo Inaayat Ek Qatra Bas Asal Ka",
            "length": "6:51",
            "file": "Minnat Karo Inaayat Ek Qatra Bas Asal Ka"
        }, {
            "track": 13,
            "name": "Moharram Ni Ham SabKari Layye Ohbat",
            "length": "2:45",
            "file": "Moharram Ni Ham SabKari Layye Ohbat"
        }, {
            "track": 14,
            "name": "Radito Bema Radeya Behi Rasulullaah ",
            "length": "5:03",
            "file": "Radito Bema Radeya Behi Rasulullaah "
        }, {
            "track": 15,
            "name": "Saaqiyaan Meh Jabeen - Mein Pila Dil Nasheen",
            "length": "6:02",
            "file": "Saaqiyaan Meh Jabeen - Mein Pila Dil Nasheen"
        }, {
            "track": 16,
            "name": "Saiful Huda Ki Ye Milaad Se Hein Shaadmaani ",
            "length": "6:03",
            "file": "Saiful Huda Ki Ye Milaad Se Hein Shaadmaani "
        }, {
            "track": 17,
            "name": "Tujhe Jab Shahe Behr O Bar Dekhte Hein",
            "length": "7:06",
            "file": "Tujhe Jab Shahe Behr O Bar Dekhte Hein"
        }, {
            "track": 18,
            "name": "Tujhsa Paida Ho Kahan Eik Hazaaro Ke Bich",
            "length": "5:30",
            "file": "Tujhsa Paida Ho Kahan Eik Hazaaro Ke Bich"
        }],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});