// html5media enables <video> and <audio> tags in all major browsers
// External File: http://api.html5media.info/1.1.8/html5media.min.js


// Add user agent as an attribute on the <html> tag...
// Inspiration: http://css-tricks.com/ie-10-specific-styles/
var b = document.documentElement;
b.setAttribute('data-useragent',  navigator.userAgent);
b.setAttribute('data-platform', navigator.platform );


// HTML5 audio player + playlist controls...
// Inspiration: http://jonhall.info/how_to/create_a_playlist_for_html5_audio
// Mythium Archive: https://archive.org/details/mythium/
jQuery(function ($) {
    var supportsAudio = !! document.createElement('audio').canPlayType;
    if (supportsAudio) {
        var index = 0,
            playing = false;
        mediaPath = 'https://archive.org/download/Qassidaa_201510/',
        extension = '',
        tracks = [{
            "track": 1,
            "name": "Coming Soon !",
            "length": "02:46",
            "file": "Q"
        }, {
            "track": 2,
            "name": "chehlam_ni_majlis_rachani",
            "length": "08:30",
            "file": "Qassidaa2"
        }, {
            "track": 3,
            "name": "Ae-maula-teri-ek-nigah-ae-karam",
            "length": "05:01",
            "file": "Qassidaa3"
        }, {
            "track": 4,
            "name": "bawa_bawa_kahu_mein_pukari",
            "length": "08:32",
            "file": "Qassidaa4"
        }, {
            "track": 5,
            "name": "saifulhuda_muki_dunya_ne_chala",
            "length": "05:05",
            "file": "Qassidaa5"
        }, {
            "track": 6,
            "name": "Deeno-Dunya",
            "length": "02:49",
            "file": "Qassidaa6"
        }, {
            "track": 7,
            "name": "saifedi_maula_taiyeb_na_pyara",
            "length": "05:44",
            "file": "Qassidaa7"
        }, {
            "track": 8,
            "name": "deen-e-haq-na-aa-gulshan-na-upar",
            "length": "05:27",
            "file": "Qassidaa8"
        }, {
            "track": 9,
            "name": "Dil-Me-Teri-Soorat-Hai",
            "length": "05:46",
            "file": "Qassidaa9"
        }, {
            "track": 10,
            "name": "Jeene-Me-Nahi-Lazzat-Maula",
            "length": "05:25",
            "file": "Qassidaa10"
        }, {
            "track": 11,
            "name": "ae_taher_e_mutahar",
            "length": "05:53",
            "file": "Qassidaa11"
        }, {
            "track": 12,
            "name": "Haae-Mashki-Ko-Liye",
            "length": "04:41",
            "file": "Qassidaa12"
        }, {
            "track": 13,
            "name": "chalo_raudat_tahera",
            "length": "13:16",
            "file": "Qassidaa13"
        }, {
            "track": 14,
            "name": "Deeno-Dunya",
            "length": "08:13",
            "file": "Qassidaa14"
        }, {
            "track": 15,
            "name": "Ae Allad na dai",
            "length": "07:02",
            "file": "Qassidaa15"
        }, {
            "track": 16,
            "name": "Ae Aqa Maula Hamne muki kaha gaya cho",
            "length": "05:44",
            "file": "Qassidaa16"        }],
        trackCount = tracks.length,
        npAction = $('#npAction'),
        npTitle = $('#npTitle'),
        audio = $('#audio1').bind('play', function () {
            playing = true;
            npAction.text('Now Playing...');
        }).bind('pause', function () {
            playing = false;
            npAction.text('Paused...');
        }).bind('ended', function () {
            npAction.text('Paused...');
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                audio.play();
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }).get(0),
        btnPrev = $('#btnPrev').click(function () {
            if ((index - 1) > -1) {
                index--;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        btnNext = $('#btnNext').click(function () {
            if ((index + 1) < trackCount) {
                index++;
                loadTrack(index);
                if (playing) {
                    audio.play();
                }
            } else {
                audio.pause();
                index = 0;
                loadTrack(index);
            }
        }),
        li = $('#plList li').click(function () {
            var id = parseInt($(this).index());
            if (id !== index) {
                playTrack(id);
            }
        }),
        loadTrack = function (id) {
            $('.plSel').removeClass('plSel');
            $('#plList li:eq(' + id + ')').addClass('plSel');
            npTitle.text(tracks[id].name);
            index = id;
            audio.src = mediaPath + tracks[id].file + extension;
        },
        playTrack = function (id) {
            loadTrack(id);
            audio.play();
        };
        extension = audio.canPlayType('audio/mpeg') ? '.mp3' : audio.canPlayType('audio/ogg') ? '.ogg' : '';
        loadTrack(index);
    }
});